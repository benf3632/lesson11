#include "threads.h"

void I_Love_Threads()
{
	cout << "I love Threads" << endl;
}

void call_I_Love_Threads()
{
	thread t1(I_Love_Threads);
	t1.join();
}

/*
prints vector
*/
void printVector(vector<int> primes)
{
	for (int i = 0; i < primes.size(); i++)
	{
		cout << primes[i] << endl;
	}
}

/*
gets prime numbers betwen a range
*/
void getPrimes(int begin, int end, vector<int>& primes)
{
	bool notPrime = false;
	for (int i = begin; i <= end; i++)
	{
		notPrime = false;
		if (i == 2 || i == 0) //avoid 2 or 0 in range
		{
			notPrime = true;
		}
		for (int j = 2; j <= sqrt(i) && notPrime == false; j++) //runs until gets to the square root of the number or if it is already not a prime
		{

			if (sqrt(i) == 2 || i % j == 0)
			{
				notPrime = true;
			}
		}

		if (!notPrime)
		{
			primes.push_back(i); //pushes to the vector
		}
	}
}

/*
Runs getPrimes using a thread
*/
vector<int> callGetPrimes(int begin, int end)
{
	vector<int> primes;
	clock_t startT = clock(); //gets the clcok before the thread started
	thread t(getPrimes,ref(begin), ref(end), ref(primes));
	t.join();
	cout << "Run time is: " << (double)(clock() - startT) << " ms" << endl; //prints to screen run time of a thread
	return primes;
}

void writePrimesToFile(int begin, int end, ofstream & file)
{
	bool notPrime = false;
	for (int i = begin; i <= end; i++)
	{
		notPrime = false;
		if (i == 2 || i == 0) //avoids 2 or 0 in range
		{
			notPrime = true;
		}
		for (int j = 2; j <= sqrt(i) && notPrime == false; j++) //stops until reached square root or if already not prime
		{
			
			if (sqrt(i) == 2 || i % j == 0)
			{
				notPrime = true;
			}
		}

		if (!notPrime)
		{
			file << i << endl; //inserts to the file
		}
	}
}

/*
get primes with threads by spliting them to N threads and insterts the primes to a file
*/
void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{
	ofstream file;
	vector<thread> threads;
	file.open(filePath);
	
	int split = end / N, tempBegin = begin, tempEnd = begin + split;

	if (file.is_open()) //checks if the file opened
	{
		clock_t startT = clock(); //get start time of the current task
		for (int i = 0; i < N; i++)
		{
			threads.push_back(thread(writePrimesToFile, ref(tempBegin), ref(tempEnd), ref(file))); //creates a new thread

			//calculates the spliting of the ranges
			if (i + 1 == N - 1)
			{
				tempBegin = tempEnd + 1;
				tempEnd = end;
			}
			else
			{
				tempBegin = tempEnd + 1;
				tempEnd += split;
			}
		}

		//waits for all the threads
		for (int i = 0; i < N; i++)
		{
			threads[i].join();
		}

		cout << "Run time is: " << (double)(clock() - startT) << " ms" <<  endl; //prints to the screen the total run time of the task
		file.close();
	}
	
}
